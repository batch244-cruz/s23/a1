

let trainer = {}
console.log(trainer);

trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasur"];
trainer.friends = {
	kanto: ['Brock', 'Misty'],
	hoenn: ['May', 'Max']
};

trainer.talk = function() {
	console.log('Pikachu, I choose you!');
};
console.log(trainer);
console.log("Result of dot notaion");
console.log(trainer.name);
console.log('Result of sqaure bracket notation:');
console.log(trainer['pokemon']);
console.log('Result of talk method:');
trainer.talk();

// Constructor Function
function Pokemon(name, level) {

	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//Methods
	this.tackle = function(target) {
		console.log(this.name + "tackled" + target.name);
		target.health -= this.attack;
		console.log(target.name + "'s health is not reduced to " + target.health);

		if (target.health <= 0){
			target.faint();
		}
	}
		this.faint = function() {
		console.log(this.name + " fainted.")
	}
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewto = new Pokemon("Mewto", 100);
console.log(mewto);

geodude.tackle(pikachu);
console.log(pikachu);

mewto.tackle(geodude);
console.log(geodude);


